﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TreeEditorAPI.Exceptions;
using TreeEditorAPI.Interfaces;
using TreeEditorAPI.Models;
using TreeEditorAPI.Models.Result;

namespace TreeEditorAPI.Controllers
{
    [Route("api/Tree")]
    [ApiController]
    public class TreeController : ControllerBase
    {
        private ITreeService _service;
        

        public TreeController(ITreeService service)
        {
            _service = service;
        }

        
        
        [HttpGet]
        public async Task<ActionResult<TreeActionResult<List<TreeModel>>>> GetTrees()
        {
            var result = new TreeActionResult<List<TreeModel>>();
            try
            {
                var trees = await _service.GetTreesAsync();
                if(trees != null)
                {
                    trees.ForEach( x => { 
                         result.Item.Add(new ViewTreeModel(x));
                    });
                }
                result.Success = true;
                Response.StatusCode = 200 ;
            }
            catch (TreeException tex)
            {
                Response.StatusCode = 400;
                result.Success = false;
                result.Message = tex.Message;
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
                Response.StatusCode = 500;
                result.Success = false;
                result.Message = "An error occured";
            }
            return result;
        }


        // POST api/values
        [HttpPost]
        public async Task<ActionResult<TreeActionResult<EditTreeModel>>> UpsertTree([FromBody] EditTreeModel model)
        {
            var result = new TreeActionResult<EditTreeModel>();
            
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new TreeException(String.Join(',', ModelState.Values.SelectMany(x => x.Errors.Select(y => y.ErrorMessage))));
                }
                else
                {
                    var treeEntity = model.ToTreeEntity();
                    if (await _service.ValidateTreeAsync(treeEntity))
                    {
                        result.Item = new EditTreeModel(await _service.UpsertTreeAsync(treeEntity));
                        result.Success = true;
                        Response.StatusCode = 200;
                    }
                }
            }
            catch (TreeException tex)
            {
                Response.StatusCode = 400;
                result.Success = false;
                result.Message = tex.Message;
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
                Response.StatusCode = 500;
                result.Success = false;
                result.Message = "An error occured";
            }
            return result;
        }


        
        [HttpDelete]
        public async Task<ActionResult<TreeActionResult<bool>>> DeleteTree([FromBody] DeleteTreeModel model)
        {
            var result = new TreeActionResult<bool>();
            try
            {   
                    result.Item = await _service.DeleteTreeAsync(model.TreeID);
                    result.Success = true;
                    Response.StatusCode = 200;
               
            }
            catch (TreeException tex)
            {
                Response.StatusCode = 400;
                result.Success = false;
                result.Message = tex.Message;
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
                Response.StatusCode = 500;
                result.Success = false;
                result.Message = "An error occured";
            }
            return result;
        }
    }
}
