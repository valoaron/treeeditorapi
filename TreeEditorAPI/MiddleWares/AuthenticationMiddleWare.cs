﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeEditorAPI.Configs;

namespace TreeEditorAPI.MiddleWares
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IOptions<TreeConfig> conf)
        {
            string authHeader = context.Request.Headers["Authorization"];
            if (authHeader != null )
            {
                //Extract credentials
                // VHJlZUFkbWlufDEyMzQ1 ->  TreeAdmin|12345
                string usernamePassword = Encoding.Default.GetString((Convert.FromBase64String(authHeader)));

                int seperatorIndex = usernamePassword.IndexOf('|');

                var username = usernamePassword.Substring(0, seperatorIndex);
                var password = usernamePassword.Substring(seperatorIndex + 1);

                if (username == conf.Value.UserName && password == conf.Value.Password)
                {
                    await _next.Invoke(context);
                }
                else
                {
                    context.Response.StatusCode = 401; //Unauthorized
                    return;
                }
            }
            else
            {
                // no authorization header, startup page
                
                await _next.Invoke(context);
                
            }
        }
    }
}
