﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TreeEditorAPI.Entities;

namespace TreeEditorAPI.Interfaces
{
    public interface ITreeService
    {
         Task<Tree> UpsertTreeAsync(Tree item);
         Task<bool> DeleteTreeAsync(int treeId);
         Task<List<Tree>> GetTreesAsync();
         Task<bool> ValidateTreeAsync(Tree item);

    }
}
