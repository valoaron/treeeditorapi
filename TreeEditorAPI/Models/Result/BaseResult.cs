﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TreeEditorAPI.Models.Result
{
    public class BaseResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }

     
    }
}
