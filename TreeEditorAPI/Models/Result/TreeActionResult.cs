﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TreeEditorAPI.Models.Result
{
    public class TreeActionResult<T> : BaseResult where T : new()
    {
        public T Item { get; set; }

        public TreeActionResult()
        {
            this.Item = new T();
        }

    }
}
