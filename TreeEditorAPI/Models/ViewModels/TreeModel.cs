﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TreeEditorAPI.Entities;
using TreeEditorAPI.Models.Result;

namespace TreeEditorAPI.Models
{
    public abstract class TreeModel 
    {
        public int TreeID { get; set; }
        [Required]
        public DateTime PlantDate { get; set; }
        public int Age { get; }
        public decimal Thickness { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public bool IsDeleted { get; set; }
        public int TypeID { get; set; }

        public TreeModel()
        {
            this.IsDeleted = false;
        }

        public TreeModel(Tree tree)
        {
            if (tree != null)
            {
                this.TreeID = tree.TreeID;
                this.PlantDate = tree.PlantDate;
                this.Thickness = tree.Thickness;
                this.Latitude = tree.Latitude;
                this.Longitude = tree.Longitude;
                this.Age = tree.Age;
                this.TypeID = tree.TypeID;
                this.IsDeleted = tree.IsDeleted;
            }
            else
            {
                this.IsDeleted = false;
            }
        }

        public Tree ToTreeEntity()
        {
            var result = new Tree();
            result.Longitude = this.Longitude;
            result.Latitude = this.Latitude;
            result.TreeID = this.TreeID;
            result.PlantDate = this.PlantDate;
            result.IsDeleted = this.IsDeleted;
            result.TypeID = this.TypeID;
            result.Thickness = this.Thickness;
            return result;
        }
    }
}
