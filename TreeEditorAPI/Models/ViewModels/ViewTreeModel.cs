﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TreeEditorAPI.Entities;

namespace TreeEditorAPI.Models.Result
{
    public class ViewTreeModel : TreeModel
    {

        public ViewTreeModel(Tree tree) : base(tree)
        {

        }
    }
}
