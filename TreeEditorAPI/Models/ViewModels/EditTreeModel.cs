﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TreeEditorAPI.Entities;

namespace TreeEditorAPI.Models
{
    public class EditTreeModel : TreeModel
    {

        public EditTreeModel(Tree tree) : base(tree)
        {

        }

        public EditTreeModel()
        {
            this.IsDeleted = false;
        }
    }
}
