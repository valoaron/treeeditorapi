﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TreeEditorAPI.Contexts;
using TreeEditorAPI.Entities;
using TreeEditorAPI.Exceptions;
using TreeEditorAPI.Interfaces;

namespace TreeEditorAPI.Services
{
    public class TreeService : ITreeService
    {
        private TreeDBContext _ctx;

        public TreeService(TreeDBContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<List<Tree>> GetTreesAsync()
        {
            return await _ctx.Trees.ToListAsync();
        }

        public async Task<bool> DeleteTreeAsync(int treeId)
        {
            bool result = false;
            var tree = await _ctx.Trees.SingleOrDefaultAsync(x => x.TreeID == treeId);
            if (tree == null)
                throw new TreeException("Tree was not found!");
            else
            {
                tree.IsDeleted = true;
                _ctx.Trees.Update(tree);
                await _ctx.SaveChangesAsync();
                result = true;
            }
            return result;
        }

        public async Task<Tree> UpsertTreeAsync(Tree item)
        {
            Tree result = item;
            if (item.TreeID <= 0)
            {
                item.IsDeleted = false;
                item.Type = GetTreeType(item.TypeID);
                _ctx.Trees.Add(item);
            }
            else
            {
                var tree = await _ctx.Trees.SingleOrDefaultAsync(x => x.TreeID == item.TreeID);
                if (tree == null)
                {
                    throw new TreeException("Tree was not found!");
                }
                UpdateTreeProperties(item, tree);
                tree.Type = GetTreeType(tree.TypeID);
                _ctx.Trees.Update(tree);

            }
            await _ctx.SaveChangesAsync();
            return result;
        }

        public async Task<bool> ValidateTreeAsync(Tree item)
        {
            var result = true;
            if (item.PlantDate == null)
                throw new TreeException("The plant date is required!");
            
            if (item.TypeID == 0)
                throw new TreeException("The plant type is required!");


            var type = await _ctx.TreeTypes.SingleOrDefaultAsync(x => x.TreeTypeID == item.TypeID);
            if (type == null)
                throw new TreeException("The given plant type is not valid!");

            return result;
        }

        private TreeType GetTreeType(int id)
        {
            var type= _ctx.TreeTypes.SingleOrDefault(x => x.TreeTypeID == id);
            if (type == null)
                throw new TreeException("Treetype was nout found!");
            return type;
        }

        private void UpdateTreeProperties(Tree sourceTree, Tree targetTree)
        {
            targetTree.TypeID = sourceTree.TypeID;
            targetTree.IsDeleted = sourceTree.IsDeleted;
            targetTree.Latitude = sourceTree.Latitude;
            targetTree.Longitude = sourceTree.Longitude;
            targetTree.PlantDate = sourceTree.PlantDate;
            targetTree.Thickness = sourceTree.Thickness;
        }


    }
}
