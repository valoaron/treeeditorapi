﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TreeEditorAPI.Entities
{
    public class Tree
    {
        public int TreeID { get; set; }
        public DateTime PlantDate { get; set; }
        public TreeType Type { get; set; }
        public int TypeID {get;set;}
        public int Age
        {
            get
            {
                if (PlantDate != null)
                    return  DateTime.Now.Year - PlantDate.Year;
                return 0;
            }
        }
        public decimal Thickness { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public bool IsDeleted { get; set; }

        
    }
}
