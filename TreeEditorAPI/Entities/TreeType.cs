﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TreeEditorAPI.Entities
{
    public class TreeType
    {
        public int TreeTypeID { get; set; }
        public string Name { get; set; }
    }
}
