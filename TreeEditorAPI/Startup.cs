﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using TreeEditorAPI.Configs;
using TreeEditorAPI.Contexts;
using TreeEditorAPI.Filters;
using TreeEditorAPI.Interfaces;
using TreeEditorAPI.MiddleWares;
using TreeEditorAPI.Services;

namespace TreeEditorAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<TreeDBContext>(options => options.UseInMemoryDatabase(databaseName: "TreeDatabase"));
            services.AddScoped<ITreeService, TreeService>();
            services.AddSwaggerGen(c =>
            {
                c.OperationFilter<AuthorizationHeader>();
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "TreeAPI",
                    Description = "Tree editor API",
                  
                  
                });
            });

            services.Configure<TreeConfig>(Configuration.GetSection("TreeConfig"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "TreeAPI V1");
                });
            }
            else
            {
                app.UseHsts();
            }
            
            app.UseMiddleware<AuthenticationMiddleware>();
            app.UseHttpsRedirection();
            app.UseMvc();
         
        }
    }
}
