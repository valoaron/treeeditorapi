﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TreeEditorAPI.Exceptions
{
    public class TreeException : Exception
    {
        public TreeException(string message) : base(message)
        {

        }
    }
}
