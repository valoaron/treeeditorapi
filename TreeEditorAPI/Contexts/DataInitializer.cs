﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TreeEditorAPI.Contexts
{
    public class DataInitializer
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new TreeDBContext(
                serviceProvider.GetRequiredService<DbContextOptions<TreeDBContext>>()))
            {
                if (context.TreeTypes.Any())
                {
                    return;   // DB was initialized already
                }

                context.TreeTypes.Add(new Entities.TreeType() { TreeTypeID = 1, Name = "Cherry" });
                context.TreeTypes.Add(new Entities.TreeType() { TreeTypeID = 2, Name = "Apple" });
                context.TreeTypes.Add(new Entities.TreeType() { TreeTypeID = 3, Name = "Pear" });
                context.TreeTypes.Add(new Entities.TreeType() { TreeTypeID = 4, Name = "Plum" });
                context.SaveChanges();
            }
        }
    }
}
