﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TreeEditorAPI.Entities;

namespace TreeEditorAPI.Contexts
{
    public class TreeDBContext : DbContext
    {
        public TreeDBContext(DbContextOptions<TreeDBContext> options)
       : base(options)
        {
        }

        public DbSet<Tree> Trees { get; set; }

        public DbSet<TreeType> TreeTypes { get; set; }
    }
}
